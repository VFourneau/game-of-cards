import flask

from flask import request, jsonify
from Game import Game
from Deck import Deck

app = flask.Flask(__name__)
games = []
decks = []

def get_game(game_id):
    for game in games:
        if game.game_id == game_id:
            return game

def get_deck(deck_id):
    for deck in decks:
        if deck.deck_id == deck_id:
            return deck

@app.route('/game/new', methods=['GET'])
def game_new():
    game = Game()
    json_game = {"game_id": game.game_id, "message": "Game is now open"}
    games.append(game)
    return jsonify(json_game)

@app.route('/game/end', methods=['GET'])
def game_end():
    if 'game_id' in request.args:
        game_id = str(request.args['game_id'])
    else:
        return "Error: No game ID provided!"
    game = get_game(game_id)
    if game == None:
        return "Error: No game with this ID exists, please try again!"

    games.remove(game)
    json_game = {"game_id": game_id, "message": "Game is now closed"}
    return jsonify(json_game)

@app.route('/game/deck/add', methods=['GET'])
def game_add_deck():
    if 'game_id' in request.args:
        game_id = str(request.args['game_id'])
    else:
        return "Error: No game ID provided!"
    game = get_game(game_id)
    if game == None:
        return "Error: No game with this ID exists, please try again!"

    if 'deck_id' in request.args:
        deck_id = str(request.args['deck_id'])
        deck = get_deck(deck_id)
        if deck == None:
            return "Error: No deck with this ID exists, please try again!"
        game.shoe.add_deck(deck)
    else:
        deck_id = game.shoe.add_deck().deck_id
    json_deck = {"game_id": game.game_id, "deck_id": deck_id, "message": "Deck has been added to the game"}

    return jsonify(json_deck)

@app.route('/game/player/add', methods=['GET'])
def game_player_add():
    if 'game_id' in request.args:
        game_id = str(request.args['game_id'])
    else:
        return "Error: No game ID provided!"
    game = get_game(game_id)
    if game == None:
        return "Error: No game with this ID exists, please try again!"

    player = game.add_player()
    json_player = {"game_id": game.game_id, "player_id": player.player_id, "Message": "Player entered the game"}

    return jsonify(json_player)

@app.route('/game/player/del', methods=['GET'])
def game_player_del():
    if 'game_id' in request.args:
        game_id = str(request.args['game_id'])
    else:
        return "Error: No game ID provided!"
    if 'player_id' in request.args:
        player_id = str(request.args['player_id'])
    else:
        return "Error: No player ID provided!"

    game = get_game(game_id)
    if game == None:
        return "Error: No game with this ID exists, please try again!"
    try:
        game.del_player(player_id) 
    except:
        return "Error: This player doesn´t exist"
    json_player = {"game_id": game.game_id, "player_id": player_id, "Message": "Player left the game"}
    return jsonify(json_player)

@app.route('/game/player/cards/list', methods=['GET'])
def game_player_cards_list():
    if 'game_id' in request.args:
        game_id = str(request.args['game_id'])
    else:
        return "Error: No game ID provided!"
    if 'player_id' in request.args:
        player_id = str(request.args['player_id'])
    else:
        return "Error: No player ID provided!"

    game = get_game(game_id)
    if game == None:
        return "Error: No game with this ID exists, please try again!"

    player = game.get_player(player_id)
    if player == None:
        return "Error: No player with this ID exists, please try again!"
    cards = player.hand.list_cards()
    json_cards = {"game_id": game.game_id, "player_id": player.player_id, "cards": cards}
    return jsonify(json_cards)

@app.route('/game/player/cards/sort', methods=['GET'])
def game_player_cards_sort():
    if 'game_id' in request.args:
        game_id = str(request.args['game_id'])
    else:
        return "Error: No game ID provided!"
    if 'player_id' in request.args:
        player_id = str(request.args['player_id'])
    else:
        return "Error: No player ID provided!"

    game = get_game(game_id)
    if game == None:
        return "Error: No game with this ID exists, please try again!"

    player = game.get_player(player_id)
    if player == None:
        return "Error: No player with this ID exists, please try again!"
    player.hand.sort()
    cards = player.hand.list_cards()
    json_cards = {"game_id": game.game_id, "player_id": player.player_id, "cards": cards, "message": "cards have been sorted"}
    return jsonify(json_cards)

@app.route('/game/player/cards/deal', methods=['GET'])
def game_player_cards_deal():
    if 'game_id' in request.args:
        game_id = str(request.args['game_id'])
    else:
        return "Error: No game ID provided!"
    if 'player_id' in request.args:
        player_id = str(request.args['player_id'])
    else:
        return "Error: No player ID provided!"
    if 'count' in request.args:
        try:
            count = int(request.args['count'])
        except ValueError:
            return "Error: Please enter a number equal to or greater than 1"
    else:
        count = 1
    game = get_game(game_id)
    if game == None:
        return "Error: No game with this ID exists, please try again!"

    player = game.get_player(player_id)
    if player == None:
        return "Error: No player with this ID exists, please try again!"
    if count > game.shoe.count():
        return "Error: Not enough cards in the shoe to deal this amount of cards"
    if count < 1:
        return "Error: You can only deal one or more cards"
    cards = game.shoe.deal_cards(count)
    player.hand.cards.extend(cards)
    json_cards = {"game_id": game.game_id, "player_id": player.player_id, "message": "player received " + str(count) + " cards"}
    return jsonify(json_cards)

@app.route('/game/players/value', methods=['GET'])
def game_players_value():
    if 'game_id' in request.args:
        game_id = str(request.args['game_id'])
    else:
        return "Error: No game ID provided!"
    game = get_game(game_id)
    if game == None:
        return "Error: No game with this ID exists, please try again!"
    json_cards = {"game_id": game_id, "players_hands": game.get_players_value()}
    return jsonify(json_cards)

@app.route('/game/shoe/shuffle', methods=['GET'])
def game_shoe_shuffle():
    if 'game_id' in request.args:
        game_id = str(request.args['game_id'])
    else:
        return "Error: No game ID provided!"

    game = get_game(game_id)
    if game == None:
        return "Error: No game with this ID exists, please try again!"
    game.shoe.shuffle()
    json_shoe = {"game_id": game.game_id, "message": "Shoe has been suffled"}
    return jsonify(json_shoe)

@app.route('/game/shoe/suit_left', methods=['GET'])
def game_shoe_suit_left():
    if 'game_id' in request.args:
        game_id = str(request.args['game_id'])
    else:
        return "Error: No game ID provided!"

    game = get_game(game_id)
    if game == None:
        return "Error: No game with this ID exists, please try again!"

    hearts, spades, clubs, diamonds = game.shoe.left_inSuit()

    json_shoe = {"game_id": game.game_id, "hearts_left": hearts, "spades_left": spades, "clubs_left": clubs, "diamonds_left": diamonds, "message": ""}
    return jsonify(json_shoe)

@app.route('/game/shoe/count', methods=['GET'])
def game_shoe_count():
    if 'game_id' in request.args:
        game_id = str(request.args['game_id'])
    else:
        return "Error: No game ID provided!"

    game = get_game(game_id)
    if game == None:
        return "Error: No game with this ID exists, please try again!"
    cards, count = game.shoe.dedup()
    json_cards = []
    for i in range(0, len(cards)):
        json_cards.append({"card": str(cards[i]), "amount": count[i]})
    json_shoe = {"game_id": game.game_id, "cards_left": json_cards, "message": ""}
    return jsonify(json_shoe)

@app.route('/deck/shuffle', methods=['GET'])
def deck_shuffle():
    if 'deck_id' in request.args:
        deck_id = str(request.args['deck_id'])
    else:
        return "Error: No game ID provided!"
    deck = get_deck(deck_id)
    json_deck = {"deck_id": deck_id, "message": "Deck has been suffled"}

    return jsonify(json_deck)

@app.route('/deck/new', methods=['GET'])
def deck_new():
    deck = Deck()
    decks.append(deck)
    json_deck = {"deck_id": deck.deck_id, "message": "Deck is now available"}
    return jsonify(json_deck)

@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404

app.run()

