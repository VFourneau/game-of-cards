from __future__ import print_function, division
from collections import Counter, OrderedDict

import uuid
import random
import operator

class Card:
    """

    """
    suit_str = ["Hearts", "Spades", "Clubs", "Diamonds"]
    face_str = ["none", "Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"]

    def __init__(self, suit=1, face=1):
        self.suit = suit
        self.face = face
    
    def __str__(self):
        """Returns a human-readable string representation."""
        return '%s of %s' % (Card.face_str[self.face], Card.suit_str[self.suit])

    def __eq__(self, other):
        return self.suit == other.suit and self.face == other.face

    def __hash__(self):
        return hash(str(self))

    def __lt__(self, other):
        c1 = self.suit, self.face
        c2 = other.suit, other.face
        return c1 < c2

class Deck:
    def __init__(self):
        self.deck_id = str(uuid.uuid4())[:8]
        self.cards = []
        for suit in range(4):
            for face in range(1, 14):
                self.cards.append(Card(suit, face))

    def add_card(self, card):
        self.cards.append(card)

    def del_card(self, card):
        self.cards.remove(card)

    def pop_card(self, i=-1):
        return self.cards.pop(i)

    def shuffle(self):
        random.shuffle(self.cards)

    def sort(self):
        s = sorted( self.cards, key=operator.attrgetter('face'), reverse=True)
        self.cards = sorted(s, key=operator.attrgetter("suit"))

    def deal_cards(self, count):
        cards = []
        for i in range(count):
            cards.append(self.pop_card())
        return cards

    def get_cards_value(self):
        value = 0
        for card in self.cards:
            value += card.face
        return value

    def list_cards(self):
        cards = []
        for card in self.cards:
            cards.append(str(card))
        return cards
    
    def left_inSuit(self):
        """
        returns a tuple (hearts, spades, clubs, diamonds) where each item is an int representing the amount left of of each suit in the deck
        """
        hearts = 0
        spades = 0
        clubs = 0
        diamonds = 0
        for card in self.cards:
            if card.suit == 0:
                hearts += 1
            elif card.suit == 1:
                spades += 1
            elif card.suit == 2:
                clubs += 1
            else:
                diamonds += 1
        return hearts, spades, clubs, diamonds                                           

    def count(self):
        return len(self.cards)

class Shoe(Deck):
    def add_deck(self, deck=Deck()):
        self.cards.extend(deck.cards)
        return deck

    def dedup(self):
        """
        Convert the shoe into an ordered Dictionary then count each occurence of the card.
        returns a tuple (key , value) where key and value are lists
        """
        tmp = self
        tmp.sort()
        cnt = Counter(tmp.cards)
        key = list(cnt.keys())
        value = list(cnt.values())

        return key, value
       
class Hand(Deck):
    def __init__(self):
        self.cards = []

    def add_cards(self, cards):
        self.cards.extend(cards)
    