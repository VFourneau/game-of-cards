# game of cards

Assignment by LogMeIn

## API calls

### GET

#### Starting a game

Path: `/game/new`  

Parameter: N/A  

Action: Create a new game

Return value: 

```json
{
  "game_id": "1acb1f21",
  "message": "Game is now open"
}
```

#### Ending a game

Path: `/game/end`  

Parameter:  

- `game_id` [required]

Action: Terminate a game  

Return value: 

```json
{
  "game_id": "7153766e",
  "message": "Game is now closed"
}
```

#### Adding a deck

Path: `/game/deck/add`

Parameter: 
- `game_id` [required]
- `deck_id` [optional]

Action: Adds a new deck or a specific deck to the game's shoe

Return value: 

```json
{
  "deck_id": "40bb04f5",
  "game_id": "7b2eb0b5",
  "message": "Deck has been added to the game"
}
```

#### Adding a player

Path: `/game/player/add`

Parameter: 

- `game_id` [required]

Action: Adds a player to the game

Return value: 

```json
{
  "Message": "Player entered the game",
  "game_id": "1acb1f21",
  "player_id": "4cbd6d53"
}
```

#### Deleting a player

Path: `/game/player/del`

Parameter:

- `game_id` [required]
- `player_id` [required]

Action: Removes a player from the game

Return value: 

```json
{
  "Message": "Player left the game",
  "game_id": "92f3806a",
  "player_id": "58a1817f"
}
```

#### Listing a player's card

Path: `/game/player/cards/list`

Parameter: 

- `game_id` [required]
- `player_id` [required]

Action: List the cards for that player

Return value: 

```json
{
  "cards": [
    "3 of Clubs",
    "Jack of Hearts",
    "7 of Diamonds",
    "Ace of Hearts",
    "5 of Diamonds"
  ],
  "game_id": "1acb1f21",
  "player_id": "9e6dcd50"
}
```

#### Sort a player's cards

Path: `/game/player/cards/sort`

Parameter:

- `game_id` [required]
- `player_id` [required]

Action: Sort the player's cards

Return value: 

```json
{
  "cards": [
    "King of Diamonds",
    "Queen of Diamonds",
    "Jack of Diamonds",
    "10 of Diamonds",
    "9 of Diamonds"
  ],
  "game_id": "fcab6013",
  "message": "cards have been sorted",
  "player_id": "132f81c1"
}
```

#### Dealing cards to a player

Path: `/game/player/cards/deal`

Parameter: 

- `game_id` [required]
- `player_id` [required]
- `count` [optional]

Action: Deals `count` cards to the player. By default, deals one card

Return value: 

```json
{
  "game_id": "fcab6013",
  "message": "player received 5 cards",
  "player_id": "132f81c1"
}
```

#### Player's value

Path: `/game/players/value`

Parameter: 

- `game_id` [required]

Action: List the face values of each player's hand

Return value: 

```json
{
  "game_id": "fcab6013",
  "players_hands": [
    [
      "132f81c1",
      55
    ]
  ]
}
```

#### Counting cards

Path: `/game/shoe/count`

Parameter: `game_id` [required]

Action: Count the number of occurence left of each card in the shoe

Return value: 

```json
{
  "cards_left": [
    {
      "amount": 1,
      "card": "King of Hearts"
    },
    {
      "amount": 2,
      "card": "Queen of Hearts"
    },
    ...
    {
      "amount": 1,
      "card": "Ace of Diamonds"
    }
  ],
  "game_id": "bae586ae",
  "message": ""
}
```

#### Creating a deck

Path: `/deck/new`

Parameter: N/A

Action: Create a new deck

Return value: 

```json
{
  "deck_id": "f42fb7fe",
  "message": "Deck is now available"
}
```

#### Shuffling a deck

Path: `/deck/shuffle`

Parameter:

- `deck_id` [required]

Action: Shuffles a deck

Return value: 

```json
{
  "deck_id": "d7aa06a3",
  "message": "Deck has been suffled"
}
```