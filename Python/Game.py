import sys
import uuid

sys.path.append(".")

from Deck import Deck, Hand, Shoe

class Player:

    def __init__(self):
        self.hand = Hand()
        self.player_id = str(uuid.uuid4())[:8]
        return

class Game:

    def __init__(self):
        self.players = []
        self.shoe = Shoe()
        self.game_id = str(uuid.uuid4())[:8]
        return

    def add_player(self):
        player = Player()
        self.players.append(player)
        return player
    
    def del_player(self, player_id):
        self.players.remove(self.get_player(player_id))

    def get_players(self):
        return self.players
    
    def get_player(self, player_id):
        for player in self.players:
            if player.player_id == player_id:
                return player
    
    def get_players_value(self):
        values = []
        for player in self.players:
            values.append((player.player_id, player.hand.get_cards_value()))
        values.sort(key = lambda x: x[1])
        return values